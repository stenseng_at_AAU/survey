#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Apr 20 10:45:04 2020

@author: Lars Stenseng
"""

import unittest
from survey.observation.point import Points
from survey.observation.point import Height,  Position #, Position3D
from survey.observation.observation import Observations
from survey.observation.observation import HeightDiff, Vector2D #, Vector3D
# from survey.observation.observation import TotalObs, Local2D, Local3D
from survey.adjustment.adjustment import Adj1D, Adj2D


class TestAdjustmentClass(unittest.TestCase):

    def kmStdDev(dist):
        return 0.005 * (dist/1000.0) ** 0.5

    @classmethod
    def setUpClass(cls):
        cls._obs = Observations()
        cls._pntKnown = Points()

        cls._pntKnown.addPoint(Height('A', 8.130, stdHeight=0.001))

        stddev1 = cls.kmStdDev(8200)
        niv1 = HeightDiff(1, 'B', 'A', 1.207, 8200, stddH=stddev1)
        cls._obs.addObservation(niv1)
        stddev2 = cls.kmStdDev(5000)
        niv2 = HeightDiff(2, 'D', 'B', 1.115, 5000, stddH=stddev2)
        cls._obs.addObservation(niv2)
        stddev3 = cls.kmStdDev(2700)
        niv3 = HeightDiff(3, 'D', 'A', 2.305, 2700, stddH=stddev3)
        cls._obs.addObservation(niv3)
        stddev4 = cls.kmStdDev(6700)
        niv4 = HeightDiff(4, 'B', 'C', 2.097, 6700, stddH=stddev4)
        cls._obs.addObservation(niv4)
        stddev5 = cls.kmStdDev(2900)
        niv5 = HeightDiff(5, 'D', 'C', 3.203, 2900, stddH=stddev5)
        cls._obs.addObservation(niv5)
        stddev6 = cls.kmStdDev(7400)
        niv6 = HeightDiff(6, 'A', 'C', 0.906, 7400, stddH=stddev6)
        cls._obs.addObservation(niv6)

        cls._pnt = cls._obs.returnCartesian()
        cls._adj = Adj1D(cls._obs, cls._pntKnown)

        # SETUP OF 2D Case
        cls._pntKnown2D = Points()
        cls._obs2D = Observations()

        # Target points
        stdTarget = 0.001
        cls._pntKnown2D.addPoint(Position('301', e=0.000, n=3.706,
                                          stdE=stdTarget, stdN=stdTarget))
        cls._pntKnown2D.addPoint(Position('302', e=18.431, n=3.131,
                                          stdE=stdTarget, stdN=stdTarget))
        cls._pntKnown2D.addPoint(Position('303', e=29.470, n=-6.809,
                                          stdE=stdTarget, stdN=stdTarget))
        cls._pntKnown2D.addPoint(Position('304', e=-11.218, n=-39.639,
                                          stdE=stdTarget, stdN=stdTarget))

        # Observations
        stdObs = 0.001
        # Observation set 1
        obs1 = Vector2D(1, 'st01', '301', dX=0.000, dY=19.660, stddX=stdObs,
                        stddY=stdObs)
        cls._obs2D.addObservation(obs1)
        obs2 = Vector2D(2, 'st01', '302', dX=17.237, dY=13.110, stddX=stdObs,
                        stddY=stdObs)
        cls._obs2D.addObservation(obs2)
        obs3 = Vector2D(3, 'st01', '303', dX=24.432, dY=0.116, stddX=stdObs,
                        stddY=stdObs)
        cls._obs2D.addObservation(obs3)
        obs4 = Vector2D(4, 'st01', '304', dX=-24.731, dY=-17.662, stddX=stdObs,
                        stddY=stdObs)
        cls._obs2D.addObservation(obs4)
        # Observation set 2
        obs5 = Vector2D(5, 'st02', '301', dX=0.000, dY=16.595, stddX=stdObs,
                        stddY=stdObs)
        cls._obs2D.addObservation(obs5)
        obs6 = Vector2D(6, 'st02', '302', dX=-9.368, dY=0.714, stddX=stdObs,
                        stddY=stdObs)
        cls._obs2D.addObservation(obs6)
        obs7 = Vector2D(7, 'st02', '303', dX=-23.393, dY=-4.181, stddX=stdObs,
                        stddY=stdObs)
        cls._obs2D.addObservation(obs7)
        obs8 = Vector2D(8, 'st02', '304', dX=-32.604, dY=47.282, stddX=stdObs,
                        stddY=stdObs)
        cls._obs2D.addObservation(obs8)
        # Observation set 3
        obs9 = Vector2D(9, 'st03', '301', dX=0.000, dY=20.096, stddX=stdObs,
                        stddY=stdObs)
        cls._obs2D.addObservation(obs9)
        obs10 = Vector2D(10, 'st03', '302', dX=9.236, dY=36.056, stddX=stdObs,
                         stddY=stdObs)
        cls._obs2D.addObservation(obs10)
        obs11 = Vector2D(11, 'st03', '303', dX=-23.219, dY=41.070,
                         stddX=stdObs, stddY=stdObs)
        cls._obs2D.addObservation(obs11)
        obs12 = Vector2D(12, 'st03', '304', dX=32.859, dY=-10.315,
                         stddX=stdObs, stddY=stdObs)
        cls._obs2D.addObservation(obs12)
        # Create class pointer
        cls._adj2d = Adj2D(cls._obs2D, cls._pntKnown2D)

    def test_returnPoints(self):
        print(len(self._obs.observations), self._obs.observations)
        print(len(self._pnt.observations), self._pnt.observations)
        print(len(self._pntKnown.points), self._pntKnown.points)

    # def test_adjustment(self):
    #     self._adj.setupDesignMatrix()
    #     print("\nDesign matrix:\n", self._adj.designMatrix)
    #     self._adj.setupObservationVector()
    #     print("\nObservation Vector Array:\n", self._adj.observationVector)
    #     self._adj.setupAprioriCovariance()
    #     print("\nA priori covariance matrix:\n", self._adj.aprioriCovariance)
    #     self._adj.setupWeightMatrix()
    #     print("\nWeight matrix, C:\n", self._adj.weightMatrix)
    #     self._adj.setupDesignMatrix()
    #     self._adj.adjustObservations()
    #     print("\nAdjusted heights, xhat:\n", self._adj.xhat)
    #     self._adj.calculateResiduals()
    #     print("\nResiduals on heights, rhat:\n", self._adj.residuals)

    # def test_adjustment2D(self):
    #     self._adj2d.countCommonPoints()
    #     #self._adj2d.setupDesignMatrix2d()
    #     print("\n2D Known points dataframe (Anblok):\n",
    #           self._pntKnown2D.points)
    #     print("\n2D Observation dataframe (Anblok):\n",
    #           self._obs2D.observations)
    #     print("\nDesign matrix 2D:\n", self._adj2d.designMatrix2d)
