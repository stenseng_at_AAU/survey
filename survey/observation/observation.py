#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Nov 04 14:44:35 2019

@author: Lars Stenseng
"""

import pandas as pd
from copy import deepcopy
from math import pi, sin, cos, atan2
from . import point as pos


class Error(Exception):
    """Base class for position exceptions"""
    pass


class ObsTypeError(Error):
    """Exceptions relatede to wrong use of observation types"""

    def __init__(self, message):
        self.message = message


class DoubletError(Error):
    """Exceptions relatede to reuse of a pointId"""

    def __init__(self, message):
        self.message = message


class OperationError(Error):
    """Exceptions relatede to use of operations"""

    def __init__(self, message):
        self.message = message


class Observation:
    def __init__(self, obsId=None, fromId=None, toId=None, epoch=-0.0,
                 obsType=None, face=None, x=None, y=None, z=None,
                 stdX=None, stdY=None, stdZ=None,
                 covXY=None, covXZ=None, covYZ=None):
        self.observation = pd.DataFrame([[obsId, fromId, toId,
                                          epoch, obsType, face,
                                          x, y, z,
                                          stdX, stdY, stdZ,
                                          covXY, covXZ, covYZ]],
                                        columns=('obsId', 'fromId', 'toId',
                                                 'epoch', 'obsType', 'face',
                                                 'x', 'y', 'z',
                                                 'stdX', 'stdY', 'stdZ',
                                                 'covXY', 'covXZ', 'covYZ'))
        return self

    def rawData(self):
        return self.observation

    def fromData(self, observationData):
        if isinstance(observationData, (HeightDiff, Vector3D, Vector2D,
                                        Local3D, Local2D, TotalObs)):
            self.observation = observationData.rawData()
        else:
            raise ObsTypeError('\nWarning! Observation type does not exist\n\n'
                               ' Use HeightDiff, Vector3D, Vector2D, Local3D, '
                               'Local2D, or TotalObs instead')
        return self


class HeightDiff:
    def __init__(self, obsId=None, fromId=None, toId=None, dH=None, dist=None,
                 epoch=-0.0, stddH=-0.0):
        self.obsId = obsId
        self.obsType = 'niv'
        self.fromId = fromId
        self.toId = toId
        self.epoch = epoch
        self.dH = dH
        self.dist = dist
        self.stddH = stddH
        self.posTol = 0.0001

    def fromRawData(self, data):
        self.obsId = data['obsId']
        self.fromId = data['fromId']
        self.toId = data['toId']
        self.epoch = data['epoch']
        self.dH = data['z']
        self.dist = data['x']
        self.stddH = data['stdZ']
        return self

    def rawData(self):
        data = pd.DataFrame([[self.obsId, self.fromId, self.toId, self.epoch,
                              self.obsType, self.dH, self.dist, self.stddH]],
                            columns=('obsId', 'fromId', 'toId', 'epoch',
                                     'obsType', 'z', 'x', 'stdZ'))
        return data

    def __repr__(self):
        return (f'{self.__class__.__name__}({self.obsId}, {self.fromId}, '
                f'{self.toId}, {self.dH}, {self.dist}, {self.epoch}, '
                f'{self.stddH})')

    def __str__(self):
        return f'({self.dH:.4f} m \u00b1{self.stddH:.4f} m)'

    def __eq__(self, other):
        if (abs(self.dH - other.dH) < self.posTol):
            return True
        else:
            return False

    def __ne__(self, other):
        if not self.__eq__(other):
            return True
        else:
            return False

    def __add__(self, other):
        result = deepcopy(self)
        if isinstance(other, float) or isinstance(other, int):
            result.dH = self.dH + other
        elif isinstance(other, self.__class__):
            result.dH = self.dH + other.dH
        return result

    def __radd__(self, other):
        return self.__add__(other)

    def __iadd__(self, other):
        return self.__add__(other)

    def __sub__(self, other):
        result = deepcopy(self)
        if isinstance(other, float) or isinstance(other, int):
            result.dH = self.dH - other
        elif isinstance(other, self.__class__):
            result.dH = self.dH - other.dH
        return result

    def __rsub__(self, other):
        result = deepcopy(self)
        if isinstance(other, float) or isinstance(other, int):
            result.dH = other - self.dH
        elif isinstance(other, self.__class__):
            result.dH = other.dH - self.dH
        return result

    def __isub__(self, other):
        return self.__sub__(other)

    def __mul__(self, other):
        result = deepcopy(self)
        if isinstance(other, float) or isinstance(other, int):
            result.dH = self.dH * other
        else:
            raise OperationError('\nWarning! Heights can only be multiplied '
                                 ' with floats or integers')
        return result

    def __rmul__(self, other):
        return self.__mul__(other)

    def __imul__(self, other):
        return self.__mul__(other)

    def __truediv__(self, other):
        result = deepcopy(self)
        if isinstance(other, float) or isinstance(other, int):
            result.dH = self.dH / other
        else:
            raise OperationError('\nWarning! floats and integers can not be '
                                 ' devided with heights')
        return result

    def __itruediv__(self, other):
        return self.__truediv__(other)


class Vector2D:
    def __init__(self, obsId, fromId, toId, dX, dY, epoch=-0.0,
                 stddX=-0.0, stddY=-0.0, covdXdY=-0.0):
        self.obsId = obsId
        self.obsType = 'v2d'
        self.fromId = fromId
        self.toId = toId
        self.epoch = epoch
        self.dX = dX
        self.dY = dY
        self.stddX = stddX
        self.stddY = stddY
        self.covdXdY = covdXdY

    def rawData(self):
        data = pd.DataFrame([[self.obsId, self.fromId, self.toId, self.epoch,
                              self.obsType, self.dX, self.dY,
                              self.stddX, self.stddY, self.covdXdY]],
                            columns=('obsId', 'fromId', 'toId', 'epoch',
                                     'obsType', 'x', 'y',
                                     'stdX', 'stdY', 'covXY'))
        return data

    def __repr__(self):
        return (f'{self.__class__.__name__}({self.obsId}, {self.fromId}, '
                f'{self.toId}, {self.dX}, {self.dY}, {self.epoch}, '
                f'{self.stddX}, {self.stddY})')

    def __str__(self):
        return (f'({self.dX:.4f} m \u00b1{self.stddX:.4f} m)\n'
                f'({self.dY:.4f} m \u00b1{self.stddY:.4f} m)\n')

    def __eq__(self, other):
        if (abs(self.dX - other.dX) < self.posTol
                and abs(self.dY - other.dY) < self.posTol):
            return True
        else:
            return False

    def __ne__(self, other):
        if not self.__eq__(other):
            return True
        else:
            return False

    def __add__(self, other):
        result = deepcopy(self)
        if isinstance(other, self.__class__):
            result.dX = self.dX + other.dX
            result.dY = self.dY + other.dY
        else:
            raise OperationError('\nWarning! Vector2D can only be added '
                                 'with a 2D object')
        return result

    def __radd__(self, other):
        return self.__add__(other)

    def __iadd__(self, other):
        return self.__add__(other)

    def __sub__(self, other):
        result = deepcopy(self)
        if isinstance(other, self.__class__):
            result.dX = self.dX - other.dX
            result.dY = self.dY - other.dY
        else:
            raise OperationError('\nWarning! Vector2D can only be subtracted '
                                 'from a 2D object')
        return result

    def __rsub__(self, other):
        result = deepcopy(self)
        if isinstance(other, self.__class__):
            result.dX = self.dX - other.dX
            result.dY = self.dY - other.dY
        else:
            raise OperationError('\nWarning! Vector2D can only be subtracted '
                                 'from a 2D object')
        return result

    def __isub__(self, other):
        return self.__sub__(other)

    def __mul__(self, other):
        result = deepcopy(self)
        if isinstance(other, float) or isinstance(other, int):
            result.dX = self.dX * other
            result.dY = self.dY * other
        else:
            raise OperationError('\nWarning! Vector2D can only be multiplied '
                                 ' with floats or integers')
        return result

    def __rmul__(self, other):
        return self.__mul__(other)

    def __imul__(self, other):
        return self.__mul__(other)

    def __truediv__(self, other):
        result = deepcopy(self)
        if isinstance(other, (float, int)):
            result.dX = self.dX / other
            result.dY = self.dY / other
        else:
            raise OperationError('\nWarning! floats and integers can not be '
                                 ' devided with Vector2D')
        return result

    def __itruediv__(self, other):
        return self.__truediv__(other)


class Vector3D:
    def __init__(self, obsId, fromId, toId, dX, dY, dZ, epoch=-0.0,
                 stddX=-0.0, stddY=-0.0, stddZ=-0.0,
                 covdXdY=-0.0, covdXdZ=-0.0, covdYdZ=-0.0):
        self.obsId = obsId
        self.obsType = 'v3d'
        self.fromId = fromId
        self.toId = toId
        self.epoch = epoch
        self.dX = dX
        self.dY = dY
        self.dZ = dZ
        self.stddX = stddX
        self.stddY = stddY
        self.stddZ = stddZ
        self.covdXdY = covdXdY
        self.covdXdZ = covdXdZ
        self.covdYdZ = covdYdZ

    def rawData(self):
        data = pd.DataFrame([[self.obsId, self.fromId, self.toId, self.epoch,
                              self.obsType, self.dX, self.dY, self.dZ,
                              self.stddX, self.stddY, self.stddZ,
                              self.covdXdY, self.covdXdZ, self.covdYdZ]],
                            columns=('obsId', 'fromId', 'toId', 'epoch',
                                     'obsType', 'x', 'y', 'z',
                                     'stdX', 'stdY', 'stdZ',
                                     'covXY', 'covXZ', 'covYZ'))
        return data

    def __repr__(self):
        return (f'{self.__class__.__name__}({self.obsId}, {self.fromId}, '
                f'{self.toId}, {self.dX}, {self.dY}, {self.dZ}, {self.epoch}, '
                f'{self.stddX}, {self.stddY}, {self.stddZ})')

    def __str__(self):
        return (f'({self.dX:.4f} m \u00b1{self.stddX:.4f} m,'
                f' {self.dY:.4f} m \u00b1{self.stddY:.4f} m,'
                f' {self.dZ:.4f} m \u00b1{self.stddZ:.4f} m)\n')

    def __eq__(self, other):
        if (abs(self.dX - other.dX) < self.posTol
                and abs(self.dY - other.dY) < self.posTol
                and abs(self.dZ - other.dZ) < self.posTol):
            return True
        else:
            return False

    def __ne__(self, other):
        if not self.__eq__(other):
            return True
        else:
            return False

    def __add__(self, other):
        result = deepcopy(self)
        if isinstance(other, self.__class__):
            result.dX = self.dX + other.dX
            result.dY = self.dY + other.dY
            result.dZ = self.dZ + other.dZ
        else:
            raise OperationError('\nWarning! Vector3D can only be added '
                                 'with a 3D object')
        return result

    def __radd__(self, other):
        return self.__add__(other)

    def __iadd__(self, other):
        return self.__add__(other)

    def __sub__(self, other):
        result = deepcopy(self)
        if isinstance(other, self.__class__):
            result.dX = self.dX - other.dX
            result.dY = self.dY - other.dY
            result.dZ = self.dZ - other.dZ
        else:
            raise OperationError('\nWarning! Vector3D can only be subtracted '
                                 'from a 3D object')
        return result

    def __rsub__(self, other):
        result = deepcopy(self)
        if isinstance(other, self.__class__):
            result.dX = self.dX - other.dX
            result.dY = self.dY - other.dY
            result.dZ = self.dZ - other.dZ
        else:
            raise OperationError('\nWarning! Vector3D can only be subtracted '
                                 'from a 3D object')
        return result

    def __isub__(self, other):
        return self.__sub__(other)

    def __mul__(self, other):
        result = deepcopy(self)
        if isinstance(other, float) or isinstance(other, int):
            result.dX = self.dX * other
            result.dY = self.dY * other
            result.dZ = self.dZ * other
        else:
            raise OperationError('\nWarning! Vector3D can only be multiplied '
                                 ' with floats or integers')
        return result

    def __rmul__(self, other):
        return self.__mul__(other)

    def __imul__(self, other):
        return self.__mul__(other)

    def __truediv__(self, other):
        result = deepcopy(self)
        if isinstance(other, float) or isinstance(other, int):
            result.dX = self.dX / other
            result.dY = self.dY / other
            result.dZ = self.dZ / other
        else:
            raise OperationError('\nWarning! floats and integers can not be '
                                 ' devided with Vector3D')
        return result

    def __itruediv__(self, other):
        return self.__truediv__(other)


class TotalObs:
    def __init__(self, obsId=None, fromId=None, toId=None,
                 az=None, ze=None, dist=None, face=1, epoch=-0.0,
                 stdAz=-0.0, stdZe=-0.0, stdDist=-0.0,
                 covAzZe=-0.0, covAzDist=-0.0, covZeDist=-0.0):
        self.obsId = obsId
        self.obsType = 'tot'
        self.fromId = fromId
        self.toId = toId
        self.epoch = epoch
        self.az = az
        self.ze = ze
        self.dist = dist
        self.face = face
        self.stdAz = stdAz
        self.stdZe = stdZe
        self.stdDist = stdDist
        self.covAzZe = covAzZe
        self.covAzDist = covAzDist
        self.covZeDist = covZeDist

    def __gon2rad(self, angle: float) -> float:
        """Convert gon to radians"""
        return angle * pi / 200.0

    def __rad2gon(self, angle: float) -> float:
        return angle * 200.0 / pi

    def fromRawData(self, data):
        self.obsId = data['obsId']
        self.fromId = data['fromId']
        self.toId = data['toId']
        self.epoch = data['epoch']
        self.obsType = data['obsType']
        self.face = data['face']
        self.az = data['x']
        self.ze = data['y']
        self.dist = data['z']
        self.stdAz = data['stdX']
        self.stdZe = data['stdY']
        self.stdDist = data['stdZ']
        self.covAzZe = data['covXY']
        self.covAzDist = data['covXZ']
        self.covZeDist = data['covYZ']
        return self

    def rawData(self):
        data = pd.DataFrame([[self.obsId, self.fromId, self.toId, self.epoch,
                              self.obsType, self.face,
                              self.az, self.ze, self.dist,
                              self.stdAz, self.stdZe, self.stdDist,
                              self.covAzZe, self.covAzDist, self.covZeDist]],
                            columns=('obsId', 'fromId', 'toId', 'epoch',
                                     'obsType', 'face', 'x', 'y', 'z',
                                     'stdX', 'stdY', 'stdZ',
                                     'covXY', 'covXZ', 'covYZ'))
        return data

    def changeFace(self, newFace):
        if self.face != newFace:
            if self.face == 1:
                self.face = 2
            else:
                self.face = 1
            self.az = (self.az + 200.0) % 400.0
            self.ze = 400.0 - self.ze
        return self

    def returnVector3D(self) -> Vector3D:
        """Return local cartesian coordinates calculated from TotalObs"""
        if self.face == 2:
            self.changeFace(1)
        vector3D = Vector3D(self.obsId, self.fromId, self.toId, 0.0, 0.0, 0.0,
                            self.epoch)
        planDist = sin(self.__gon2rad(self.ze)) * self.dist
        vector3D.dX = planDist * sin(self.__gon2rad(self.az))
        vector3D.dY = planDist * cos(self.__gon2rad(self.az))
        vector3D.dZ = cos(self.__gon2rad(self.ze)) * self.dist

        # Check the errorpropagation!!!
        stdPlanDist = (self.stdDist * sin(self.__gon2rad(self.ze)) ** 2) ** 0.5
        print(stdPlanDist)
        vector3D.stddX = ((stdPlanDist *
                           cos(self.__gon2rad(self.az))) ** 2) ** 0.5
        vector3D.stddY = ((stdPlanDist *
                           sin(self.__gon2rad(self.az))) ** 2) ** 0.5
        vector3D.stddZ = (self.stdDist *
                          cos(self.__gon2rad(self.ze)) ** 2) ** 0.5

        return vector3D

    def __repr__(self):
        return (f'{self.__class__.__name__}({self.obsId}, {self.fromId}, '
                f'{self.toId}, {self.az}, {self.ze}, {self.dist}, {self.face},'
                f' {self.epoch}, {self.stdAz}, {self.stdZe}, {self.stddist})')

    def __str__(self):
        return (f'({self.az:.4f} gon \u00b1{self.stdAz:.4f} gon,'
                f' {self.ze:.4f} gon \u00b1{self.stdZe:.4f} gon,'
                f' {self.dist:.4f} m \u00b1{self.stdDist:.4f} m)\n')


class Local2D(Vector2D):
    def __init__(self, obsId, localSys, toId, x, y, epoch=-0.0,
                 stdX=-0.0, stdY=-0.0, covXY=-0.0):
        super().__init__(obsId, localSys, toId, x, y, epoch,
                         stdX, stdY, covXY)
        self.obsType = 'l2d'


class Local3D(Vector3D):
    def __init__(self, obsId, localSys, toId, x, y, z, epoch=-0.0,
                 stdX=-0.0, stdY=-0.0, stdZ=-0.0,
                 covXY=-0.0, covXZ=-0.0, covYZ=-0.0):
        super().__init__(obsId, localSys, toId, x, y, z, epoch,
                         stdX, stdY, stdZ, covXY, covXZ, covYZ)
        self.obsType = 'l3d'


class Observations:
    def __init__(self):
        self.observations = pd.DataFrame(columns=['obsId', 'fromId', 'toId',
                                                  'epoch', 'obsType', 'face',
                                                  'x', 'y', 'z',
                                                  'stdX', 'stdY', 'stdZ',
                                                  'covXY', 'covXZ', 'covYZ'])
        self.index = 0

    def __iter__(self):
        return self

    def __next__(self):
        observation = Observation()
        try:
            observation.observation = self.observations.iloc[[self.index]]
        except IndexError:
            raise StopIteration
        self.index += 1
        return observation

    def addObservation(self, observation):
        if observation.obsId not in self.observations.obsId.to_numpy():
            self.observations = self.observations\
                                    .append(observation.rawData(),
                                            ignore_index=True,
                                            sort=False)\
                                    .sort_values(by='obsId', ignore_index=True)
        else:
            raise DoubletError('\nWarning! Observation already exists\n\n'
                               ' Use another obsId or replaceObservation()')

    def returnCartesian(self):
        cartObservations = Observations()
        fromPoints = pd.DataFrame(self.observations[['fromId', 'obsType',
                                                     'epoch']].
                                  rename(columns={'fromId': 'id'}))
        toPoints = pd.DataFrame(self.observations[['toId', 'obsType', 'epoch']].
                                rename(columns={'toId': 'id'}))
        points = fromPoints.append(toPoints, ignore_index=True)\
            .drop_duplicates()
        for index, obs in self.observations.iterrows():
            if (obs['obsType'] != 'tot') and (obs['obsType'] != 'niv'):
                cartObservations.observations =\
                    cartObservations.observations.append(obs)
            elif obs['obsType'] == 'tot':
                totObs = TotalObs()
                v3obs = totObs.fromRawData(obs).returnVector3D()
                cartObservations.addObservation(v3obs)
            else:
                nivObs = HeightDiff()
                niv = nivObs.fromRawData(obs)
                cartObservations.addObservation(niv)
        return cartObservations
