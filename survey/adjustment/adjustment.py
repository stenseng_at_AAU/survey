#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Apr 20 10:43:12 2020

@author: Lars Stenseng
"""

from survey.observation.point import Points  # DimensionError, DoubletError
# from survey.observation.point import Height, Position, Position3D
from survey.observation.observation import Observations  # , DoubletError
from survey.observation.observation import HeightDiff, Vector3D  # Vector2D,
# from survey.observation.observation import TotalObs, Local2D, Local3D
import numpy as np
import pandas as pd


class Adj1D:

    def __init__(self, observations: Observations,
                 knownPoints: Points) -> None:
        self.observations = observations
        self.knownPoints = knownPoints
        self.points = observations.returnCartesian()
        self.residuals = np.zeros(0)
        self.aprioriCovariance = np.zeros(0)
        self.weightMatrix = np.zeros(0)
        self.apostioriCovariance = np.zeros(0)
        self.designMatrix = np.zeros(0)
        self.column = {}
        self.numberObservations = len(self.observations.observations)
        self.numberPoints = len(self.points.observations)
        self.numberKnownPoints = len(self.knownPoints.points)

    def setupDesignMatrix(self):
        self.designMatrix.resize((self.numberObservations
                                  + self.numberKnownPoints, self.numberPoints))

        # for observation in self.observations.observations:
        #     if isinstance(observation, (HeightDiff, Vector3D)):
        #         self.usedObservations.append(observation)
        #     else:
        #         pass

        # for index, pointName in enumerate(self.points.observations['toId']):
        #     self.column.update({pointName: index})

        # for row in range(self.numberObservations):
        #     for fromId, toId in\
        #         zip(self.observations.observations['fromId'][row],
        #             self.observations.observations['toId'][row]):
        #         self.designMatrix[row][self.column[fromId]] = -1
        #         self.designMatrix[row][self.column[toId]] = 1
        # for row in range(self.numberKnownPoints):
        #     self.designMatrix[row + self.numberObservations]\
        #                       [self.column[self.knownPoints.points['pointId']
        #                                   [row]]] = 1
        return self

    def setupObservationVector(self):
        self.observationVector = self.observations.observations['z'].to_numpy()
        for row in range(self.numberKnownPoints):
            self.observationVector = np.insert(self.observationVector,
                                               len(self.observationVector),
                                               self.knownPoints.
                                               points['height'][row])
        return self

    def setupAprioriCovariance(self):
        self.aprioriCovariance.resize(self.numberObservations +
                                      self.numberKnownPoints,
                                      self.numberObservations +
                                      self.numberKnownPoints)

        self.stdDeviations = np.concatenate((self.observations.
                                             observations['stdZ'].to_numpy(),
                                             self.knownPoints.points['stdH']
                                             .to_numpy()))
        np.fill_diagonal(self.aprioriCovariance, self.stdDeviations**2)
        return self

    def setupWeightMatrix(self, covariance=None):
        if covariance is None:
            covariance = self.aprioriCovariance

        self.weightMatrix.resize(self.numberObservations +
                                 self.numberKnownPoints,
                                 self.numberObservations +
                                 self.numberKnownPoints)
        self.weightMatrix = np.linalg.inv(covariance)
        return self

    def calculateResiduals(self):
        self.residuals = self.designMatrix.dot(self.xhat)\
                         - self.observationVector
        return self

    """ Solve the system xhat = inv(A’*C*A)*A’*C*b to find the adjusted
        heights"""

    def adjustObservations(self):

        self.xhat = ((((np.linalg.inv(self.designMatrix.transpose()
                                      .dot(self.weightMatrix)
                                      .dot(self.designMatrix)))
                                      .dot(self.designMatrix.transpose()))
                                      .dot(self.weightMatrix))
                                      .dot(self.observationVector))
        return self


class Adj2D:

    def __init__(self, observations: Observations,
                 knownPoints: Points) -> None:
        self.observations = observations
        self.knownPoints = knownPoints
        self.points = observations.returnCartesian()
        self.designMatrix2d = np.zeros(0)
        self.weightMatrix = np.zeros(0)
        self.observationVector = np.zeros(0)

        countPoints = 5 # len(coordObs['pointId'].unique())
        countCoordinates = countPoints * 2
        countModels = 5 # len(coordObs['stationId'].unique()) - 1
        countUnknown = countModels * 4 + countPoints * 2

    def countCommonPoints(self):
        self.uniquePoints = 5 # pd.concat([self.knownPoints.points['pointId'],
        #                                     self.observations.
        #                                     observations['toId']])
        self.countPoints = 5 # len(self.uniquePoints.unique())
        self.countCoordinates = self.countPoints * 2
        self.countModels = 3 # len(uniquePoints)
        self.countUnknown = self.countModels * 4 + self.countPoints * 2
        print(f'''\nCommon points: {self.countPoints}
              Models: {self.countModels}
              Unknowns: {self.countUnknown}''')
        return self

    def setupDesignMatrix2d(self):
        self.designMatrix2d.resize((self.countCoordinates * self.countModels
                                   + self.countCoordinates, self.countUnknown))

        np.fill_diagonal(self.designMatrix2d[:self.countCoordinates,
                                             self.countModels * 4:], 1)
        np.fill_diagonal(self.designMatrix2d[self.countCoordinates:
                                             self.countCoordinates * 2,
                                             self.countModels * 4:], -1)
        np.fill_diagonal(self.designMatrix2d[self.countCoordinates * 2:
                                             self.countCoordinates * 3,
                                             self.countModels * 4:], -1)
        np.fill_diagonal(self.designMatrix2d[self.countCoordinates * 3:,
                                             self.countModels * 4:], -1)
        return self
